#include <iostream>
#include <SDL2/SDL.h>
#include <cirion/engine.hpp>

int main( int argc, char *argv[] )
{
	cirion::Engine engine;

	if( engine.init() != EXIT_SUCCESS )
	{
		engine.quit();
		return EXIT_FAILURE;
	}

	engine.run();
	engine.quit();

	return EXIT_SUCCESS;
}
