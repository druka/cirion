/*
 * This file is part of Cirion.
 *
 * Cirion, a 2D side-scrolling game engine.
 * Copyright (C) 2016 Qwoak
 *
 * Cirion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cirion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file engine.cpp
 * @brief Coeur du moteur.
 */

#include <iostream>
#include <SDL2/SDL.h>
#include <cirion/engine.hpp>

/**
 * @brief Constructeur de la classe Engine.
 */
cirion::Engine::Engine() :
	mIsRunning( false ),
	mWindowPtr( nullptr ),
	mRendererPtr( nullptr )
{}

/**
 * @brief Destructeur de la classe Engine.
 */
cirion::Engine::~Engine()
{
	if( mWindowPtr   != nullptr ) SDL_DestroyWindow( mWindowPtr );
	if( mRendererPtr != nullptr ) SDL_DestroyRenderer( mRendererPtr );
}

/**
 * @brief Proc�dure d'initialisation du moteur.
 * @return Status d'ex�cution.
 */
int cirion::Engine::init()
{
	// Initialisation de la SDL2
	if( SDL_Init( SDL_INIT_VIDEO ) != 0 )
	{
		std::cerr << "SDL2 initialization failed: "
		          << SDL_GetError();

		return EXIT_FAILURE;
	}

	// Cr�ation de la fen�tre SDL2.
	mWindowPtr = SDL_CreateWindow( "SDL2",
	                               SDL_WINDOWPOS_CENTERED,
	                               SDL_WINDOWPOS_CENTERED,
	                               640,
	                               480,
	                               SDL_WINDOW_HIDDEN );

	if( mWindowPtr == nullptr )
	{
		std::cerr << "Window creation failed: "
		          << SDL_GetError();

		return EXIT_FAILURE;
	}

	// Cr�ation du renderer.
	mRendererPtr = SDL_CreateRenderer( mWindowPtr,
	                                   -1,
	                                   SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );

	if ( mRendererPtr == nullptr )
	{
		std::cerr << "SDL2 renderer creation failed: "
		          << SDL_GetError();

		return EXIT_FAILURE;
	}

	// D�finition de la taille du renderer.
	SDL_RenderSetLogicalSize( mRendererPtr,
	                          320,
	                          240 );

	// Affichage de la fen�tre.
	SDL_ShowWindow( mWindowPtr );
	// Nettoyage du renderer.
	SDL_SetRenderDrawColor( mRendererPtr, 0x00, 0x00, 0x00, 0xFF );
	SDL_RenderClear( mRendererPtr );
	// Actualisation.
	SDL_RenderPresent( mRendererPtr );

	return EXIT_SUCCESS;
}

/**
 * @brief Proc�dure de tra�tement des �v�nements.
 */
void cirion::Engine::handleEvents()
{
	// R�cuperation des �v�nements.
	while( SDL_PollEvent( &mEvent ) )
	{
		switch( mEvent.type )
		{
			case SDL_QUIT:
				mIsRunning = false;
				break;
		}
	}
}

/**
 * @brief Proc�dure de boucle principale du moteur.
 */
void cirion::Engine::run()
{
	mIsRunning = true;

	while( mIsRunning )
	{
		handleEvents();
		SDL_Delay( 1 );
	}
}

/**
 * @brief Proc�dure de sortie.
 */
void cirion::Engine::quit()
{
	SDL_Quit();
}
