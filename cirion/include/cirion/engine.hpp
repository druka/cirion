/*
 * This file is part of Cirion.
 *
 * Cirion, a 2D side-scrolling game engine.
 * Copyright (C) 2016 Qwoak
 *
 * Cirion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cirion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file engine.hpp
 * @brief Coeur du moteur.
 */

#ifndef CIRION_ENGINE_HPP_
#define CIRION_ENGINE_HPP_

#include <SDL2/SDL.h>

namespace cirion
{
	/**
	 * @class Engine engine.hpp
	 * @brief Le moteur Cirion.
	 */
	class Engine
	{
	public:
		Engine();
		~Engine();
		int init();
		void handleEvents();
		void run();
		void quit();

	private:
		bool mIsRunning;            //!< Drapeau de boucle.
		SDL_Window* mWindowPtr;     //!< La fen�tre SDL2.
		SDL_Renderer* mRendererPtr; //!< Le renderer SDL2.
		SDL_Event mEvent;           //!< La structure d'�v�nement SDL2.
	};
}

#endif /* CIRION_ENGINE_HPP_ */
